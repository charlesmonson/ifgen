import ipaddress
import re
from flask import Flask
from flask import render_template
from flask import request
from slugify import slugify
from ifgen import normalize_cid

app = Flask(__name__)

@app.route('/')
def interface():
    return render_template('interface.html')

@app.route('/result', methods = ['POST', 'GET'])
def result():
    if request.method == 'POST':
        result = request.form.to_dict()
        nocidnorm = request.form.getlist('nocidnorm')

        slug = slugify(result['service_description'])
        result['service_description'] = slug

        if nocidnorm:
            norm_cid = normalize_cid(result['cid'], normalize=False)
        else:
            norm_cid = normalize_cid(result['cid'])
        result['cid'] = norm_cid

        if re.search('\/[0-9]+\.[0-9]+', result['interface']):
            intf_encap = re.search('\/[0-9]+\.([0-9]+)', result['interface'])
            encap = intf_encap.group(1)
            result['encapsulation'] = encap

        if result['remote_hostname']:
            slug = slugify(result['remote_hostname'])
            result['remote_hostname'] = slug
        if result['remote_interface']:
            lowername = result['remote_interface'].lower()
            result['remote_interface'] = lowername
        if result['ipv6_address']:
            prefix = ipaddress.IPv6Interface(result['ipv6_address'])
            result['ipv6_address'] = prefix
        if result['ipv4_address']:
            prefix = ipaddress.IPv4Interface(result['ipv4_address'])
            mask = prefix.with_netmask.split('/')[1]
            result['ipv4_address'] = prefix
            result['ipv4_mask'] = mask
        return render_template('result.html', result = result)

if __name__ == '__main__':
    app.run(debug = True)
