from slugify import slugify
import re

def get_cid():
    while True:
        cid = input('Please enter a circuit ID: ')

        if len(cid) != 6:
            print(f"This doesn't look like a 6 digit circuit ID. Would you like to keep it or enter again?")

            choice = input(f"(K)eep, (R)etry: ")
            choice = choice.lower()

            if choice in ('k', 'keep'):
                return cid
            else:
                continue
        elif not cid.isdigit():
            print(f"This doesn't look like a 6 digit circuit ID. Would you like to keep it or enter again?")

            choice = input(f"(K)eep, (R)etry: ")
            choice = choice.lower()

            if choice in ('k', 'keep'):
                return cid
            else:
                continue
        elif not cid:
            print(f"You didn't enter a circuit ID! This shouldn't happen.")

            choice = input(f"Fully type (Continue), if this is what you intended\n or (R)etry: ")
            choice = choice.lower()

            if choice in ('continue'):
                return cid
            else:
                continue
        else:
            return cid

def get_name():
    name = input('Please enter service name: ')
    slug = slugify(name)
    return slug

def get_stype():
    return 'cust'

def normalize_cid(cid, normalize=True):
    if normalize is False:
        return cid
    else:
        try:
            result = re.search('[0-9]{6}', cid)
        except:
            result = False
        return result.group()

if __name__ == '__main__':
    cid = get_cid()
    name = get_name()
    stype = get_stype()

    print(f'  description {cid} | {stype}:{name}')
